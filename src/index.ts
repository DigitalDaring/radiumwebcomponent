export enum radiumComponentObservedAttributes {
    percent = 'percent'
}

export class RadiumComponent extends HTMLElement {
    shadow: ShadowRoot;
    currentPercent = 0;
    radiumParent: HTMLElement;

    constructor() {
        super();
        this.shadow = this.attachShadow({mode: 'open'});
        this.render(this.currentPercent);
        this.radiumParent = this.shadow.querySelector('.radium-parent') as HTMLElement;
    }

    static get observedAttributes() {
        return ['percent'];
    }

    attributeChangedCallback(name: string, oldValue: string, newValue: string){
        if(oldValue !== newValue) {
            switch(name) {
                case radiumComponentObservedAttributes.percent :
                    this.currentPercent = parseInt(newValue);
                    const rotationValue = this.currentPercent / 100 * 180;
                    this.radiumParent.style.setProperty('--circle-rotation', `${rotationValue}`);
                    break;
            }
        }
    }

    private cssStyles(percent: number): string {
        const rotation = percent / 100 * 180;
        return  `
            <style>
                .radium-parent {
                    --primary-color: var(--radium-primary-color, #97a71d);
                    --text-color: var(--radium-text-color, --primary-color);
                    background-color: var(--radium-background-color, #CCC);
                    border-radius: 50%;
                    border: 1px solid var(--radium-outer-border-color, transparent);
                    --circle-size: var(--radium-cirle-size, 120);
                    --circle-rotation: ${rotation};
                    --circle-transform: calc(var(--circle-rotation) * 1deg);
                    --circle-fix-transform: calc(var(--circle-rotation) * 2deg);
                    --full-size: calc(var(--circle-size) * 1px);
                    --half-size: calc(var(--circle-size) / 2 * 1px);
                    --font-height: var(--radium-font-height, 22);
                    --font-size: calc(var(--font-height) * 1px);
                    --text-width: var(--radium-text-width, 44);
                    --percentage-complete: calc(var(--circle-rotation) * 100 / 180);
                    width: var(--full-size);
                    height: var(--full-size);
                    position: relative;
                }
                
                .radium-parent .circle .inner-shadow {
                    width: var(--full-size);
                    height: var(--full-size);
                    position: absolute;
                    border-radius: 50%;
                    box-shadow: 4px 4px 8px var(--radium-outer-shadow-color-a, rgba(0, 0, 0, 0.2)) inset, -4px -4px 8px var(--radium-outer-shadow-color-b, transparent);
                }
                
                .radium-parent .circle .mask {
                    width: var(--full-size);
                    height: var(--full-size);
                    position: absolute;
                    clip: rect(0, var(--full-size), var(--full-size), var(--half-size));
                }
                
                .radium-parent .circle .mask.full {
                    transition: transform 1s;
                    transform: rotate(var(--circle-transform))
                }
                
                .radium-parent .circle .mask .fill {
                    background-color: var(--primary-color);
                    --webkit-backface-visibility: hidden;
                    width: var(--full-size);
                    height: var(--full-size);
                    transition: transform 1s;
                    position: absolute;
                    border-radius: 50%;
                    clip: rect(0, var(--half-size), var(--full-size), 0);
                    transform: rotate(var(--circle-transform));
                }
                
                .radium-parent .circle .mask .fill.fix {
                    transform: rotate(var(--circle-fix-transform));
                }
                
                .radium-parent .inset {
                    --inner-circle-size: calc(var(--circle-size) * .75);
                    border: 1px solid var(--radium-center-border-color, transparent);
                    width: calc(var(--inner-circle-size) * 1px);
                    height: calc(var(--inner-circle-size) * 1px);
                    position: absolute;
                    left: var(--radium-inset-left-override, calc(calc(var(--circle-size) - var(--inner-circle-size)) /2 * 1px));
                    top: var(--radium-inset-top-override, calc(calc(var(--circle-size) - var(--inner-circle-size)) /2 * 1px));
                    border-radius: 50%;
                    background-color: var(--radium-center-color, #fbfbfb);
                    box-shadow: 4px 4px 8px var(--radium-center-shadow-color-a, rgba(0,0,0,0.2)), -4px -4px 8px var(--radium-center-shadow-color-b, transparent);
                }
                
                .radium-parent .inset .percentage {
                    color: var( --text-color);
                    height: var(--font-size);
                    transition: width 1s;
                    line-height: 1;
                    width: calc(var(--text-width) * 1px);
                    overflow: hidden;
                    position: absolute;
                    top: calc(calc(var(--inner-circle-size) - var(--font-height)) / 2 * 1px);
                    left: calc(calc(var(--inner-circle-size) - var(--text-width)) / 2 * 1px);
                }
                
                .radium-parent .inset .percentage .numbers {
                    margin-top: calc(var(--font-height) / 2 * -1px);
                    transition: width 1s;
                    width: calc(calc(var(--percentage-complete) * var(--text-width) + var(--text-width)) * 1px);
                }
                
                .radium-parent .inset .percentage .numbers span {
                    width: calc(var(--text-width) * 1px);
                    display: inline-block;
                    vertical-align: top;
                    text-align: center; 
                }
                
                
            </style>
        `;
    }

    private percentages(): Array<string> {
        const allPercents = ['-'];
        for(var i = 0; i <= 100; i++) {
            allPercents.push(`${i}%`);
        }
        return allPercents;
    }

    private htmlContent(innerContent: Array<string>): string {

        const spans = innerContent.map(x => `<span>${x}</span>`).join('');

        return `
            <section class="radium-parent">
                <div class="circle">
                    <div class="mask full">
                        <div class="fill"></div>
                    </div>
                    <div class="mask half">
                        <div class="fill"></div>
                        <div class="fill fix"></div>
                    </div>
                    <div class="inner-shadow"></div>
                </div>
                <div class="inset">
                    <div class="percentage">
                        <div class="numbers">
                            ${spans}
                        </div>
                    </div>
                </div>
            </section>
        `
    }

    private render(percent: number) {
        const styles = this.cssStyles(percent);
        const html = this.htmlContent(this.percentages());
        this.shadow.innerHTML = `${styles} ${html}`;
    }

}

window.customElements.define('radium-component', RadiumComponent);