var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var RadiumComponent = (function (_super) {
    __extends(RadiumComponent, _super);
    function RadiumComponent() {
        _super.call(this);
        this.shadow = this.attachShadow({ mode: 'closed' });
        this.render();
    }
    RadiumComponent.prototype.cssStyles = function () {
        return "\n            <style>\n            </style>\n        ";
    };
    RadiumComponent.prototype.render = function () {
        var styles = this.cssStyles();
        var html = '<p>rendered! O</p>';
        this.shadow.innerHTML = styles + " " + html;
    };
    return RadiumComponent;
})(HTMLElement);
